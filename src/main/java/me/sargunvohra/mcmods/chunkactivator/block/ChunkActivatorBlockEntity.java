package me.sargunvohra.mcmods.chunkactivator.block;

import me.sargunvohra.mcmods.chunkactivator.ChunkActivator;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

// TODO This class uses a hacky ref counting solution to fix issue #2
//  this should really be replaced with storing data on the chunk itself or something better idk
//  but this works for now, I'll fix it later
//  #clowntown

public class ChunkActivatorBlockEntity extends BlockEntity implements Tickable {

    public static Map<BetterChunkPos, Integer> refCount = new HashMap<>();
    private boolean forceLoadNextTick;

    public ChunkActivatorBlockEntity() {
        super(ChunkActivator.CHUNK_ACTIVATOR_ENTITY);
    }

    private int count(int change) {
        World world = getWorld();
        if (world != null) {
            MinecraftServer server = world.getServer();
            if (server != null) {
                BlockPos pos = getPos();
                DimensionType dt = world.dimension.getType();
                BetterChunkPos key = new BetterChunkPos(new ChunkPos(pos), dt);
                int ret = refCount.getOrDefault(key, 0) + change;
                if (ret < 0) {
                    ret = 0;
                }
                refCount.put(key, ret);
                return ret;
            }
        }
        return -1;
    }

    private void forceLoad(String addOrRemove) {
        World world = getWorld();
        if (world != null) {
            MinecraftServer server = world.getServer();
            if (server != null) {
                BlockPos pos = getPos();
                String command = String.format(
                    "forceload %s %s %s", addOrRemove, pos.getX(), pos.getZ());
                server.getCommandManager().execute(server.getCommandSource(), command);
            }
        }
    }

    @Override
    public void invalidate() {
        super.invalidate();
        if (count(-1) == 0) {
            forceLoad("remove");
        }
    }

    @Override
    public void validate() {
        super.validate();
        this.forceLoadNextTick = true;
    }

    @Override
    public void tick() {
        if (this.forceLoadNextTick) {
            forceLoad("add");
            count(1);
            this.forceLoadNextTick = false;
        }
    }

    public static class BetterChunkPos {
        private ChunkPos chunkPos;
        private DimensionType dimensionType;

        BetterChunkPos(ChunkPos chunkPos, DimensionType dimensionType) {
            this.chunkPos = chunkPos;
            this.dimensionType = dimensionType;
        }

        @Override
        public String toString() {
            return String.format(
                "RealChunkPos{chunkPos=%s, dimensionType=%s}", chunkPos, dimensionType);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BetterChunkPos that = (BetterChunkPos) o;
            return Objects.equals(chunkPos, that.chunkPos)
                && Objects.equals(dimensionType, that.dimensionType);
        }

        @Override
        public int hashCode() {
            return Objects.hash(chunkPos, dimensionType);
        }
    }
}
